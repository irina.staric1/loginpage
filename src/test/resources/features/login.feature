Feature: Login Page Functionality
  As a user,
  I want to be able to log in to my account,
  So that I can access the application securely.

  Scenario: Successful Login
    Given Open login page
    When They enter valid credentials
    And Click the Login button
    Then Redirected to the account page
    And The browser will be closed

  Scenario: Unsuccessful Login
    Given Open login page
    When The User enter invalid credentials
    And Click the Login button
    Then The User is getting warning message
    And The User are on the Login page
    And The browser will be closed

  Scenario: Entering blank login fields
    Given Open login page
    And Click the Login button
    Then The User is getting warning message when the fields are empty
    And The User are on the Login page
    And The browser will be closed