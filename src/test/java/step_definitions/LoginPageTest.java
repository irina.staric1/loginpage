package step_definitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.And;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class LoginPageTest {
    private WebDriver driver;
    private LoginPage loginPage;

    @Given("Open login page")
    public void openLoginPage() {
        driver = new ChromeDriver();
        driver.manage().deleteAllCookies();
        driver.manage().window().maximize();
        driver.get("https://identity.hudl.com/login?state=hKFo2SBublFlcy1JZnoxcFp2SmUtanJ1MkxVVGQwMFFvLUtENKFupWxvZ2luo3RpZNkgUHFzYTRXRk44eGdYY09HZnhTTTJYOUg1Z0RXemg5bDCjY2lk2SBuMTNSZmtIektvemFOeFdDNWRaUW9iZVdHZjRXalNuNQ&client=n13RfkHzKozaNxWC5dZQobeWGf4WjSn5&protocol=oauth2&response_type=id_token&redirect_uri=https%3A%2F%2Fwww.hudl.com%2Fapp%2Fusers%2Foidc%2Fcallback&scope=openid%20profile%20email&nonce=bJRjE%2B10G%2Bc9eUhOAB0sd5EqYI4%2FRdIns%2F7FwbJ8avw%3D&response_mode=form_post&screen_hint=");
        loginPage = new LoginPage(driver);
    }

    @When("They enter valid credentials")
    public void enterValidCredentials() {
        loginPage.enterCredentials("irina.staric@gmail.com", "SecretPassword123/");
    }

    @When("Click the Login button")
    public void clickLoginButton() {
        loginPage.clickLoginButton();
    }

    @Then("Redirected to the account page")
    public void redirected_to_the_account_page() {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofMillis(3000));
        String expectedTitle = "Home - Hudl";
        wait.until(ExpectedConditions.titleContains(expectedTitle));
        String currentTitle = driver.getTitle();
        Assert.assertEquals(currentTitle, expectedTitle, "Title is different");
    }
// And the browser will be closed
    @And("The browser will be closed")
    public void closeBrowser() {
        driver.quit();
    }
    @When("The User enter invalid credentials")
    public void enterInvalidCredentials() {
        loginPage.enterCredentials("Username123", "password123");
    }
    @Then("The User is getting warning message")
    public void checkWarningMessage() {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        WebElement warningMessage = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//p[@class='error-message uni-text uni-text--small uni-text--set-solid']")));
        String actualWarning = warningMessage.getText();
        String expectedWarning = "We don't recognize that email and/or password";
        Assert.assertEquals(actualWarning, expectedWarning);
    }
    @And("The User are on the Login page")
    public void checkLoginPageAccess() {
        // Replace with code to check if the user is on the login page
        String currentURL = driver.getCurrentUrl();
        String expectedURL = "https://identity.hudl.com/login?state=hKFo2SA1TEhQSzBCbzgySjJxVm1mSWNDbndzQVQ0VTZsWlZTZqFupWxvZ2luo3RpZNkgVF9VRE9nNGdGYkJfLW5fVHV6aGxWT2gyWU52a08xNUajY2lk2SBuMTNSZmtIektvemFOeFdDNWRaUW9iZVdHZjRXalNuNQ&client=n13RfkHzKozaNxWC5dZQobeWGf4WjSn5&protocol=oauth2&response_type=id_token&redirect_uri=https%3A%2F%2Fwww.hudl.com%2Fapp%2Fusers%2Foidc%2Fcallback&scope=openid%20profile%20email&nonce=2tvePMaDvSra2w8Wo4rgSw7dcJ3Y49jAOqOm9x7i2Vw%3D&response_mode=form_post&screen_hint=";
        Assert.assertEquals(currentURL, expectedURL);
    }
    @Then("The User is getting warning message when the fields are empty")
    public void thenSystemShouldDisplayErrorMessage() {
        WebElement errorElement = driver.findElement(By.xpath("//p[@class='error-message uni-text uni-text--small uni-text--set-solid']"));
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofMillis(3000));
        wait.until(ExpectedConditions.visibilityOf(errorElement));
        String actualErrorMessage = errorElement.getText();
        String expectedErrorMessage = "Please fill in all of the required fields";
        Assert.assertEquals(actualErrorMessage, expectedErrorMessage, "The error message is not the same");
    }
}
