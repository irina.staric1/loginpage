This README file contains information about the prerequisites for getting started with the project, configuring the project, running the test suite, viewing test reports, troubleshooting, and contributing to the project.

Project Instructions:
The Login Page Functionality is a critical component of the project that enables user authentication and access control. It provides a secure and user-friendly way for users to access their accounts and interact with application/website. This README serves as a guide to understanding the functionality and its implementation within our project.

Features
User Authentication: Users can create accounts, log in, and reset their passwords securely.
Session Management: User sessions are managed securely to keep users logged in and authorized.
Password Encryption: User passwords are securely hashed and stored to protect user data.
Integration with Existing Systems: Easily integrate this functionality into your project with clear documentation and code examples.

Project : Framework to Test the Login Functionality
A java project using BDD with Gherkin to perform testing and generate various reports
The following requirements should be met:
* Create a Java Project for Hudl login page website.
* Add the following dependencies: selenium-java version 4.14.1, JUnit 4.13.2, TestNG 7.8.0 to the pom.xml.
* Create a TestNG class.
* Create test classes for writing the test methods.
* Create a page object design pattern class to store the web elements of web pages.
* Create Gherkin keyword (Scenario, Given, When, Then, And).
* Run the project in Google Chrome, ( please make sure that you are using the latest version of Chrome .
* Push the code to your GitHub repositories.

Introduction
The Project is dealing with UI testing, Functional, Non-Functional, TDD and DevOps integration

Setting up the Project

Dependencies: ( selenium-java version 4.14.1, JUnit 4.13.2, TestNG 7.8.0 )
Java 21
Maven
Google Chrome latest version

Getting started
How to execute the tests:

Commands will be here:
